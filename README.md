# Raspberry Pi Case Fan Control for OpenBSD
A shell script to manage the [official case fan](https://www.raspberrypi.com/products/raspberry-pi-4-case-fan/) using the OpenBSD [gpio(4)](https://man.openbsd.org/gpio) interface.

The fan will turn on when the CPU temperature is high. Unless configured otherwise, it will keep running for at least 10 minutes. That way it won't be switching on and off all the time.

There is no support for PWM (*pulse-width modulation*) -- to control the fan speed. I'm still investigating whether it's possible.

## Usage
From [gpioctl(8)](https://man.openbsd.org/gpioctl.8#DESCRIPTION):

> Only pins that have been configured at securelevel 0, typically during system startup, are accessible once the securelevel has been raised. Pins can be given symbolic names for easier use. (...)

1. Add the following to `/etc/rc.securelevel`:

```
gpioctl gpio0 14 set out case_fan
```

- Change `14` if your fan is on a different pin.
- Keep `case_fan` as the symbolic name. The script needs it.

2. Install the script in `/usr/local/bin`.

```
# cp rpi-case-fan.sh /usr/local/bin/rpi-case-fan
# chmod +x /usr/local/bin/rpi-case-fan
```

3. Add a line to the system [crontab](https://man.openbsd.org/crontab.1).

```
*/2	*	*	*	*	-n /usr/local/bin/rpi-case-fan
```

> This will run every 2 minutes.
