#!/bin/sh
# Raspberry Pi Case Fan Control for OpenBSD

THRESHOLD=60
DURATION=600
USAGE='rpi-case-fan [-t TEMPERATURE] [-d SECONDS]'

ARGS=`getopt t:d: $*`
if [ $? -ne 0 ]; then
	echo "$USAGE" >&2
	exit 2
fi
set -- $ARGS
while [ $# -ne 0 ]
do
	case "$1"
	in
		-t)
			THRESHOLD="$2"; shift; shift;;
		-d)
			DURATION="$2"; shift; shift;;
		--)
			shift; break;;
	esac
done

NOW=$(date '+%s')
VAR_RUN_DIR="/var/run/rpi-case-fan"
PIN_NAME="case_fan"

set_pin() {
	gpioctl gpio0 "$PIN_NAME" $1
}

read_temp() {
	TEMP=$(sysctl -n hw.sensors.bcmtmon0.temp0 | sed 's/ degC//')
}

#
# Get or set the last time the fan was turned on.
#
spin_up() {
	if [ "$1" != "" ]; then
		echo "$1" | tee "$VAR_RUN_DIR"/last_spin_up
	fi

	cat "$VAR_RUN_DIR"/last_spin_up
}

# Initialize the /var/run directory
if [ ! -f "$VAR_RUN_DIR"/last_spin_up ]; then
	mkdir "$VAR_RUN_DIR"
	spin_up "$NOW"
fi

read_temp

HOT=$(echo "$TEMP > $THRESHOLD" | bc)
LAST_SPIN_UP=$(spin_up)

if [ "$HOT" -eq 1 ]; then
	set_pin 1
	spin_up "$NOW"
elif [ "$((NOW - LAST_SPIN_UP))" -gt "$DURATION" ]; then
	# Will only turn it off after 10 minutes of operation
	set_pin 0
fi
